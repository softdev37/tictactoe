/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxprogram;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class OXProgram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static boolean finish = false;
    Scanner sc = new Scanner(System.in);
    static int count = 0;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table.length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);

    }

    public static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Pleaes input row, col");
        row = sc.nextInt();
        col = sc.nextInt();
    }

    public static void process() {
        if (setTable()) {
            if (checkWin()) {
                finish = true;
                showWin();
                return;
            }
            count++;
            if (checkDraw()) {
                showDraw();
            }

            switchPlayer();
        }
    }

    static boolean checkDraw() {
        if (count == 9) {
            return finish = true;

        }
        return false;

    }

    private static void showDraw() {
        showTable();
        System.out.println(">>> draw!!! <<<");

    }

    private static void showWin() {
        showTable();
        System.out.println(">>> " + currentPlayer + " win <<<");
    }

    private static boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;
    }

    private static boolean checkX1() {  //11/22/33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() { //13,22,31    =>  02,11,20
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';   //change Turn O->X
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

}
